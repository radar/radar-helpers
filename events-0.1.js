var Radar = function(radarEvents) {
  var urlMatch = radarEvents.innerHTML.match('//radar.squat.net/([a-z]{2})/events/([a-zA-Z0-9/]*)');
  if (urlMatch == null) {
    radarEvents.innerHTML += '<br/>URL not recognised. Only event searches like https://radar.squat.net/en/events/city/City_Name/group/123 supported at the moment.';
    return;
  }
  var langCode = urlMatch[1];
  var facetsArray = urlMatch[2].split('/');
  var queryUrl = 'https://radar.squat.net/api/1.2/search/events.json?';
  for (var i = 0; i < facetsArray.length; i = i + 2) {
    queryUrl += 'facets[' +  facetsArray[i] + '][]=' + facetsArray[i+1] + '&';
  }
  queryUrl += 'limit=10';
  radarEvents.innerHTML = 'loading...';

  var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
  };

  var formatEvents = function(events) {
    eventHtml = '';
    for (var eventId in events) {
      if (!events.hasOwnProperty(eventId)) continue;

      eventHtml += '<article class="radar-event">';
      startDate = new Date(events[eventId].date_time[0].time_start);
      if (events[eventId].date_time[0].time_start != events[eventId].date_time[0].time_end) {
        endDate = new Date(events[eventId].date_time[0].time_end);
      }
      else {
        endDate = false;
      }

      dateFormat = { weekday: "short", month: "short", day: "2-digit" };
      timeFormat = { hour: "2-digit", minute: "2-digit" };
      
      eventHtml += '<h1><a href=" ' + events[eventId].url  + '">' + events[eventId].title + '</a></h1>';
      startDateString = startDate.toLocaleDateString([], dateFormat);
      eventHtml += '<p class="date">' + startDateString + ' ' + startDate.toLocaleTimeString([], timeFormat);
      if (endDate) {
        eventHtml += ' - ';
        endDateString = endDate.toLocaleDateString([], dateFormat);
        if (endDateString != startDateString) {
          eventHtml += endDateString + ' ';
        }
        eventHtml += endDate.toLocaleTimeString([], timeFormat);
      }
      eventHtml += '</p>';
      if (events[eventId].category.length) {
        eventHtml += '<p class="categories">';
        var eventCategories = [];
        for (var eventCategory in events[eventId].category) {
          eventCategories.push(events[eventId].category[eventCategory].name);
        }
        eventHtml += eventCategories.join(', ');
        eventHtml += '</p>';
      }
      eventHtml += '</article>';

    }
    return eventHtml;
  }

  getJSON(queryUrl,
    function(err, data) {
      if (err !== null) {
        alert('Something went wrong: ' + err);
      } else {
        // Events object keyed by nid ends up in any order.
        eventsObject = data.result;
        eventsArray = [];
        // Sort by start date, of which there can be multiple at the same time.
        for (var event in eventsObject) {
          eventsArray.push(eventsObject[event]);
        }
        eventsArray.sort(function(a, b) { return a.date_time[0].value - b.date_time[0].value; });
        radarEvents.innerHTML = formatEvents(eventsArray);
      }
    }
  );
};

window.addEventListener("load", function(event) {
  Radar(document.getElementById('radar_events'));
});
